package com.cgtrader.spring.backend;

import java.util.List;

import java.util.ArrayList;

public class PokemonSetInfoFactory implements SetInfoFactory {

	// Data retrieved from:
	// https://bulbapedia.bulbagarden.net/wiki/List_of_Pokemon_Trading_Card_Game_expansions
	private static List<Integer> setSizes  = new ArrayList<>(); // INCLUDES secret rares
	private static List<String> setNames = new ArrayList<>();

	public PokemonSetInfoFactory() {
		setInfo();
	}

	private static void setInfo() {
		
		// TODO: [0] append comments clarifying official set count,
		// designation of secret rares (Radiant Collection/Shiny Vault), etc
		// All secrets to be appended to the end of sets
		
		// TODO: [0] create subfactories for Every. Single. Set. containing card info
		// (Subfactories to be constructed using set size, set name, official count)
		
		// TODO: [3] handle promo sets like Detective Pikachu (separate factory?)
		
		// oh boy
		// sets 1-10
		setSizes.add(0, 1); //For promos - for now
		setNames.add(0, "Promos");
		setSizes.add(1, 102);
		setNames.add(1, "Base Set");
		setSizes.add(2, 64);
		setNames.add(2, "Jungle");
		setSizes.add(3, 62);
		setNames.add(3, "Fossil");
		setSizes.add(4, 130);
		setNames.add(4, "Base Set 2");
		setSizes.add(5, 83);
		setNames.add(5, "Team Rocket");
		setSizes.add(6, 132);
		setNames.add(6, "Gym Heroes");
		setSizes.add(7, 132);
		setNames.add(7, "Gym Challenge");
		setSizes.add(8, 111);
		setNames.add(8, "Neo Genesis");
		setSizes.add(9, 75);
		setNames.add(9, "Neo Discovery");
		setSizes.add(10, 66);
		setNames.add(10, "Neo Revelation");

		// sets 11-20
		setSizes.add(11, 113);
		setNames.add(11, "Neo Destiny");
		setSizes.add(12, 110);
		setNames.add(12, "Legendary Collection");
		setSizes.add(13, 165);
		setNames.add(13, "Expedition Base Set");
		setSizes.add(14, 186);
		setNames.add(14, "Aquapolis");
		setSizes.add(15, 186);
		setNames.add(15, "Skyridge");
		setSizes.add(16, 109);
		setNames.add(16, "EX Ruby & Sapphire");
		setSizes.add(17, 100);
		setNames.add(17, "EX Sandstorm");
		setSizes.add(18, 100);
		setNames.add(18, "EX Dragon");
		setSizes.add(19, 97);
		setNames.add(19, "EX Team Magma vs Team Aqua");
		setSizes.add(20, 102);
		setNames.add(20, "EX Hidden Legends");
		
		// sets 21-30
		setSizes.add(21, 116);
		setNames.add(21, "EX FireRed & LeafGreen");
		setSizes.add(22, 111);
		setNames.add(22, "Ex Team Rocket Returns");
		setSizes.add(23, 108);
		setNames.add(23, "EX Deoxys");
		setSizes.add(24, 107);
		setNames.add(24, "EX Emerald");
		setSizes.add(25, 145);
		setNames.add(25, "EX Unseen Forces");
		setSizes.add(26, 114);
		setNames.add(26, "EX Delta Species");
		setSizes.add(27, 93);
		setNames.add(27, "EX Legend Maker");
		setSizes.add(28, 111);
		setNames.add(28, "EX Holon Phantoms");
		setSizes.add(29, 100);
		setNames.add(29, "EX Crystal Guardians");
		setSizes.add(30, 101);
		setNames.add(30, "EX Dragon Frontiers");
		
		// sets 31-40
		setSizes.add(31, 108);
		setNames.add(31, "EX Power Keepers");
		setSizes.add(32, 130);
		setNames.add(32, "Diamond & Pearl");
		setSizes.add(33, 124);
		setNames.add(33, "Mysterious Treasures");
		setSizes.add(34, 132);
		setNames.add(34, "Secret Wonders");
		setSizes.add(35, 106);
		setNames.add(35, "Great Encounters");
		setSizes.add(36, 100);
		setNames.add(36, "Majestic Dawn");
		setSizes.add(37, 146);
		setNames.add(37, "Legends Awakened");
		setSizes.add(38, 106);
		setNames.add(38, "Stormfront");
		setSizes.add(39, 133);
		setNames.add(39, "Platinum");
		setSizes.add(40, 120);
		setNames.add(40, "Rising Rivals");
		
		// sets 41-50
		setSizes.add(41, 153);
		setNames.add(41, "Supreme Victors");
		setSizes.add(42, 111);
		setNames.add(42, "Arceus");
		setSizes.add(43, 124);
		setNames.add(43, "HeartGold & SoulSilver");
		setSizes.add(44, 96);
		setNames.add(44, "Unleashed");
		setSizes.add(45, 91);
		setNames.add(45, "Undaunted");
		setSizes.add(46, 103);
		setNames.add(46, "Triumphant");
		setSizes.add(47, 106);
		setNames.add(47, "Call of Legends");
		setSizes.add(48, 115);
		setNames.add(48, "Black & White");
		setSizes.add(49, 98);
		setNames.add(49, "Emerging Powers");
		setSizes.add(50, 102);
		setNames.add(50, "Noble Victories");
		
		// sets 51-60
		setSizes.add(51, 103);
		setNames.add(51, "Next Destinies");
		setSizes.add(52, 111);
		setNames.add(52, "Dark Explorers");
		setSizes.add(53, 128);
		setNames.add(53, "Dragons Exalted");
		setSizes.add(54, 153);
		setNames.add(54, "Boundaries Crossed");
		setSizes.add(55, 138);
		setNames.add(55, "Plasma Storm");
		setSizes.add(56, 122);
		setNames.add(56, "Plasma Freeze");
		setSizes.add(57, 105);
		setNames.add(57, "Plasma Blast");
		setSizes.add(58, 140);
		setNames.add(58, "Legendary Treasures");
		setSizes.add(59, 146);
		setNames.add(59, "XY");
		setSizes.add(60, 109);
		setNames.add(60, "Flashfire");
		
		// sets 61-70
		setSizes.add(61, 113);
		setNames.add(61, "Furious Fists");
		setSizes.add(62, 122);
		setNames.add(62, "Phantom Forces");
		setSizes.add(63, 164);
		setNames.add(63, "Primal Clash");
		setSizes.add(64, 110);
		setNames.add(64, "Roaring Skies");
		setSizes.add(65, 100);
		setNames.add(65, "Ancient Origins");
		setSizes.add(66, 164);
		setNames.add(66, "BREAKthrough");
		setSizes.add(67, 123);
		setNames.add(67, "BREAKpoint");
		setSizes.add(68, 115);
		setNames.add(68, "Generations");
		setSizes.add(69, 125);
		setNames.add(69, "Fates Collide");
		setSizes.add(70, 116);
		setNames.add(70, "Steam Seige"); //Sponsored by Cyndoughquil
		
		// sets 71-80;
		setSizes.add(71, 113);
		setNames.add(71, "Evolutions");
		setSizes.add(72, 163);
		setNames.add(72, "Sun & Moon");
		setSizes.add(73, 169);
		setNames.add(73, "Guardians Rising");
		setSizes.add(74, 169);
		setNames.add(74, "Burning Shadows");
		setSizes.add(75, 78);
		setNames.add(75, "Shining Legends");
		setSizes.add(76, 124);
		setNames.add(76, "Crimson Invasion");
		setSizes.add(77, 173);
		setNames.add(77, "Ultra Prism");
		setSizes.add(78, 146);
		setNames.add(78, "Forbidden Light");
		setSizes.add(79, 183);
		setNames.add(79, "Celestial Storm");
		setSizes.add(80, 78);
		setNames.add(80, "Dragon Majesty");
		
		// sets 81-86
		setSizes.add(81, 236);
		setNames.add(81, "Lost Thunder");
		setSizes.add(82, 196);
		setNames.add(82, "Team Up");
		setSizes.add(83, 234);
		setNames.add(83, "Unbroken Bonds");
		setSizes.add(84, 258);
		setNames.add(84, "Unified Minds");
		setSizes.add(85, 163);
		setNames.add(85, "Hidden Fates");
		setSizes.add(86, 271);
		setNames.add(86, "Cosmic Eclipse");
	}

	public static List<Integer> fillSizes() {
		return setSizes;
	}

	public static List<String> fillNames() {
		return setNames;
	}

	//Get set size by set number
	@Override
	public int getSetSize(int setNumber) { 
		return setSizes.get(setNumber); 
	}
	
	//Get set size by set number
	@Override
	public int getSetSize(String setName) {
		return getSetSize(getSetNumber(setName));
	}
	
	//Get set name by set number
	@Override
	public String getSetName(int setNumber) {
		return setNames.get(setNumber);
	}
	
	public int getSetNumber(String setName) {
		int retVal = 0;
		int index = 1;
		boolean found = false;
		String searchName = setNames.get(0);
		
		//Search through the list of set names for a match, then copy
		while(!found && index < setNames.size()) {
			if(searchName.equals(setName)) {
				found = true;
				retVal = index;
			}
			else {
				index++;
				searchName = setNames.get(index);
			}
		}
		return retVal;
	}

	public int getTotalSets() {
		return 86;
	}
}
