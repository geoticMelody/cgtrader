package com.cgtrader.spring.backend;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class BackendService {

	private SQLDatabase db;
	// private String portName; //To be added later
	
	private List<Card> portfolio;
	{
		db = createNewDatabase();

		portfolio = new ArrayList<>();
		//Fill portfolio with values from database
		if(db.isEmpty())
			initPortfolio(portfolio);
		else
			portfolio = db.loadCards();
	}

	//Extremely simple init code for a single user (me!)
	private SQLDatabase createNewDatabase() {
		return new SQLDatabase("Hodgot"); //just my name for now, integration with views inc
	}
	
	// Init dummy data (for bugfixing purposes)
	private void initPortfolio(List<Card> portfolio) {
		/*
		//Cards with data to test editing; import these to fill database initially
		portfolio.add(new Card("Pokémon", "Magmar", "Unified Minds", 21, Card.NORMAL));
		portfolio.add(new Card("Pokémon", "Poliwag", "Evolutions", 23, Card.HOLO));
		portfolio.add(new Card("Pokémon", "Pidove", "Unified Minds", 174, Card.NORMAL));
		portfolio.add(new Card("Pokémon", "Houndour", "Crimson Invasion", 58, Card.REVERSE_HOLO));
		portfolio.add(new Card("Pokémon", "Larvitar", "Fates Collide", 41, Card.NORMAL));
		portfolio.add(new Card("Pokémon", "Kakuna", "Evolutions", 6, Card.NORMAL));
		portfolio.add(new Card("Pokémon", "Slobro Spirit Link", "Evolutions", 86, Card.MISPRINT));

		//Cards without data to test deletion
		portfolio.add(new Card());
		portfolio.add(new Card());
		portfolio.add(new Card());
		portfolio.add(new Card());
		*/
		
		addCard(new Card("Pokémon", "Magmar", "Unified Minds", 21, Card.NORMAL));
		addCard(new Card("Pokémon", "Poliwag", "Evolutions", 23, Card.HOLO));
		addCard(new Card("Pokémon", "Pidove", "Unified Minds", 174, Card.NORMAL));
		addCard(new Card("Pokémon", "Houndour", "Crimson Invasion", 58, Card.REVERSE_HOLO));
		addCard(new Card("Pokémon", "Larvitar", "Fates Collide", 41, Card.NORMAL));
		addCard(new Card("Pokémon", "Kakuna", "Evolutions", 6, Card.NORMAL));
		addCard(new Card("Pokémon", "Slobro Spirit Link", "Evolutions", 86, Card.MISPRINT));
		
		addCard(new Card());
		addCard(new Card());
		addCard(new Card());
		addCard(new Card());
	}

	public List<Card> getPortfolio() {
		return portfolio;
	}

	public void addCard(Card newCard) {
		/*First, add card to the database
		*addCard calls updatePorfolio, which returns
		*the new Card object after verifying its insertion
		*/
		System.out.println("Attempting to use function addCard in BackEndService:");
		portfolio.add(newCard);
		db.addCard(newCard);
	}

	public void deleteCard(Card deadCard) {
		int index = portfolio.indexOf(deadCard);
		db.deleteCard(index + 1);
		portfolio.remove(index);
	}

	public void editCard(Card oldVersion, Card newVersion) {
		int index = portfolio.indexOf(oldVersion);
		db.editCard(index + 1, newVersion);
		oldVersion.clone(newVersion);
	}
}
