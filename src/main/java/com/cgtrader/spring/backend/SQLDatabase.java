package com.cgtrader.spring.backend;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class SQLDatabase {
	private String urlRoot = "jdbc:sqlite:./databases/";
	private String url;
	private String portName;

	public SQLDatabase(String portName) {
		this.portName = portName;
		this.url = this.urlRoot + portName + ".db";

		String CREATE_PORTFOLIO_TABLE = "CREATE TABLE IF NOT EXISTS " + this.portName + " (\n"
				+ 	"id INTEGER PRIMARY KEY AUTOINCREMENT, \n"
				+ 	"game_name TEXT, \n"
				+ 	"card_name TEXT, \n"
				+ 	"set_name TEXT, \n"
				+ 	"card_num TEXT, \n"
				+ 	"property INT \n);";

		Connection conn = null;
		try{
			conn = DriverManager.getConnection(url);
			if (conn != null) {
				Statement stmt = conn.createStatement();
				stmt.execute(CREATE_PORTFOLIO_TABLE);
				System.out.println("Database created");
			}
		}
		catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		finally {
			try {
				if(conn != null)
					conn.close();
			}
			catch (SQLException e) {
				System.out.println("Failed to stop connection at database initialization");
			}
		}
	}

	public void addCard(Card card) {
		//Extract card values
		String gameName = card.getGameName();
		String cardName = card.getCardName();
		String setName = card.getSetName();
		String cardNum = card.getCardNo();
		String property = card.getProperty();
		System.out.println("Attempted to add card into portfolio named " + portName);

		//Prepare statement for database
		String INSERT_CARD = "INSERT INTO " + portName 
				+ "(game_name,card_name,set_name,card_num,property) VALUES(?,?,?,?,?)";

		Connection conn = null;
		//Send update request to database
		try{
			conn = DriverManager.getConnection(url);
			PreparedStatement prepState = conn.prepareStatement(INSERT_CARD);
			prepState.setString(1, gameName);
			prepState.setString(2, cardName);
			prepState.setString(3, setName);
			prepState.setString(4, cardNum);
			prepState.setString(5, property);
			prepState.executeUpdate();

			System.out.println("Card added to database: " + cardName);
		}
		catch (SQLException e) {
			System.out.println("Failed to add card to database");
		}
		finally {
			try {
				if(conn != null)
					conn.close();
			}
			catch (SQLException e){
				System.out.println("Failed to close connection at SQLDatabase.addCard()");
			}
		} 

		//Update the in-app portfolio with the new card
		//We do it this way to ensure that the database is readable
		//return updatePortfolio(card);
	}

	/*
	private Card updatePortfolio(Card card) {
		boolean cardConfirm = false;

		String gameName = card.getGameName();
		String cardName = card.getCardName();
		String setName = card.getSetName();
		String cardNum = card.getCardNo();
		String property = card.getProperty();


		String SELECT_CARD = "SELECT game_name, card_name, set_name, card_num, property FROM " + portName;
		//Send update request to database
		try(Connection conn = DriverManager.getConnection(url)){
			Statement state = conn.createStatement();
			ResultSet set = state.executeQuery(SELECT_CARD);

			while(!cardConfirm) {
				if(set.next()) {
					if(set.getString("game_name") == gameName
							&& set.getString("card_name") == cardName
							&& set.getString("set_name") == setName
							&& set.getString("card_num") == cardNum
							&& set.getString("property") == property) {
						cardConfirm = true;
						System.out.println("Card confirmed to be in database");
					}
				}
			}
		}
		catch (SQLException e) {
			System.out.println("Failed to update card from database");
			System.out.println(e.getMessage());
		}

		return card;
	}
	*/

	//TODO: [0] add "deleted" table for later undo functionality
	public void deleteCard(int id) {
		String DELETE_CARD = "DELETE FROM " + portName + " WHERE id = ?";

		Connection conn = null;
		try{
			conn = DriverManager.getConnection(url);
			PreparedStatement prepState = conn.prepareStatement(DELETE_CARD);
			prepState.setInt(1, id);
			prepState.executeUpdate();
			System.out.println("Card deleted successfully");
		}
		catch(SQLException e) {
			System.out.println("Failed to delete card from db.");
			System.out.println(e.getMessage());
		}
		finally {
			try {
				if(conn != null)
					conn.close();
			}
			catch(SQLException e) {
				System.out.println("Failed to stop connection at database function deleteCard()");
			}
		}
	}
	
	public void editCard(int id, Card newCard) {
		String gameName = newCard.getGameName();
		String cardName = newCard.getCardName();
		String setName = newCard.getSetName();
		String cardNum = newCard.getCardNo();
		String property = newCard.getProperty();

		String EDIT_CARD = "UPDATE " + portName + "SET game_name = ? , "
				+ "card_name = ? , "
				+ "set_name = ? , "
				+ "card_num = ? , "
				+ "property = ? "
				+ "WHERE id = ? ";

		Connection conn = null;
		try{
			conn = DriverManager.getConnection(url);
			PreparedStatement prepState = conn.prepareStatement(EDIT_CARD);

			prepState.setString(1, gameName);
			prepState.setString(2, cardName);
			prepState.setString(3, setName);
			prepState.setString(4, cardNum);
			prepState.setString(5, property);
			prepState.setInt(6, id);
			prepState.executeUpdate();
		}
		catch(SQLException e) {
			System.out.println("Failed to edit card.");
			System.out.println(e.getMessage());
		}
		finally {
			try {
				if(conn != null)
					conn.close();
			}
			catch(SQLException e) {
				System.out.println("Failed to stop connection at database function isEmpty()");
			}
		}
	}

	public List<Card> loadCards() {
		String gameName;
		String cardName;
		String setName;
		String cardNum;
		String property;

		List<Card> loadedList = new ArrayList<>();

		String LOAD_CARD = "SELECT game_name, card_name, set_name, card_num, property FROM " + portName;

		Connection conn = null;
		
		try{
			conn = DriverManager.getConnection(url);
			Statement state = conn.createStatement();
			ResultSet set = state.executeQuery(LOAD_CARD);

			while(set.next()) {
				gameName = set.getString("game_name");
				cardName = set.getString("card_name");
				setName = set.getString("set_name");
				cardNum = set.getString("card_num");
				property = set.getString("property");

				loadedList.add(new Card(gameName, cardName, setName, cardNum, property));
			}			
		}
		catch(SQLException e) {
			System.out.println("Failed to load cards.");
			System.out.println(e.getMessage());
		} 
		catch(NullPointerException e) {
			System.out.println("Empty cards deleted from " + portName);
		}
		finally {
			try {
				if(conn != null)
					conn.close();
			}
			catch(SQLException e) {
				System.out.println("Failed to close connection at SQLDatabase function loadCards()");
			}
		}

		return loadedList;
	}

	public boolean isEmpty() {
		boolean empty = false;
		String SELECT_CARD = "SELECT id FROM " + portName;
		Connection conn = null;
		//Send update request to database
		try{
			conn = DriverManager.getConnection(url);
			Statement state = conn.createStatement();
			ResultSet set = state.executeQuery(SELECT_CARD);

			if(set.next()) {
				empty = false;
			}
			else {
				empty = true;
			}
		}
		catch (SQLException e) {
			System.out.println("Failed to determine database emptiness");
		}
		finally {
			try {
				if(conn != null)
					conn.close();
			}
			catch(SQLException e) {
				System.out.println("Failed to stop connection at database function isEmpty()");
			}
		}

		return empty;
	}
}
