package com.cgtrader.spring.backend;
// import com.cgtrader.spring.backend.PokemonSetInfoFactory;

import com.vaadin.flow.component.notification.Notification;

public class Card {

	//Card properties
	public final static int NORMAL = 0;
	public final static int REVERSE_HOLO = 1;
	public final static int HOLO = 2;
	public final static int FULL_ART = 3; //Not actually necessary, useful for sorting though
	public final static int RAINBOW = 4; //Not actually necessary, useful for sorting though
	public final static int MISPRINT = 5; //bleeds, miscuts, miscenters

	private String gameName;
	private String cardName;
	private String setName;
	private String cardNo; //Accommodates RC/SV kind of identifiers

	private int setSize; //real set size, including secret rares
	//private int officialSetSize;
	private int setNumber;
	private int property; //normal/holo/full/etc
	private int quantity; //TODO: [0] add this functionality

	private SetInfoFactory setInfo;

	//TODO: [0] Validate all setters

	//Default using String cardNo
	public Card(String gameName, String cardName, String setName, String cardNo, int property) {
		this.gameName = gameName; //TODO: [0] replace with variable once other games are added
		this.cardName = cardName;
		this.setName = setName;
		this.cardNo = cardNo;
		this.property = property;

		createSetInfo(gameName);
		this.setNumber = setInfo.getSetNumber(setName);
		this.setSize = setInfo.getSetSize(setNumber); 
	}

	//Fix for accidental int cardNo (converts to String)
	public Card(String gameName, String cardName, String setName, int cardNo, int property) {
		this.gameName = gameName;
		this.cardName = cardName;
		this.setName = setName;
		this.cardNo = Integer.toString(cardNo);
		this.property = property;

		createSetInfo(gameName);
		this.setNumber = setInfo.getSetNumber(setName);
		this.setSize = setInfo.getSetSize(setNumber);
	}
	
	//Temp for for string property to int, needs a refactor badly
	public Card(String gameName, String cardName, String setName, String cardNo, String property) {
		this.gameName = gameName;
		this.cardName = cardName;
		this.setName = setName;
		this.cardNo = cardNo;
		this.property = setProperty(property);

		createSetInfo(gameName);
		this.setNumber = setInfo.getSetNumber(setName);
		this.setSize = setInfo.getSetSize(setNumber);
	}

	//Noarg blank constructor
	public Card() {
		this.cardName = "";
		this.setName = "";
		this.cardNo = "0";
		this.setSize = 0;
		this.property = NORMAL;

		this.setInfo = null;
	}

	private void createSetInfo(String gameName) {

		if(gameName.equals("Pokemon")){
			setInfo = new PokemonSetInfoFactory();
		}
		else
		{
			setInfo = new PokemonSetInfoFactory();
		}
	}

	public void clone(Card newCard) {
		if (this.gameName == newCard.gameName){
			this.cardName = newCard.cardName;
			this.setName = newCard.setName; 
			this.cardNo = newCard.cardNo; 
			this.setSize = newCard.setSize;
			this.property = newCard.property;
		}
		else {
			Notification.show("Cannot clone between games!");
		}
	}

	public String getGameName() {
		return gameName;
	}
	
	public void setGameName(String gameName) {
		this.gameName = gameName;
	}
	
	public String getCardName() {
		return cardName;
	}

	/*
	 * I intend to add verification for card names eventually, but there are two issues:
	 * > Custom cards; functionality I'm adding later for artists (fixed?)
	 * > Lack of time - there's literally thousands of cards in Pokémon alone, I can't add them all
	 */
	public void setCardName(String cardName) {
		/*if (this.setNumber != 0) //if not a promo card {
    		//verifyCard();
    	// } else
		 */
		this.cardName = cardName;
	}

	public String getSetName() {
		return setName;
	}

	public void setSetName(String setName) {
		this.setName = setName; //TODO: [3] verify through the sets factory
	}
	
	public int getSetNumber() {
		return this.setNumber;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo; //TODO: [3] verify through the sets factory
	}

	//TODO: [0] refactor this to allow for greater cohesion
	public String getProperty() {
		String retval = "";

		switch(property) {
		case NORMAL:
			retval = "Normal";
			break;
		case REVERSE_HOLO:
			retval = "Reverse Holo";
			break;
		case HOLO:
			retval = "Holo";
			break;
		case FULL_ART:
			retval = "Full art";
			break;
		case RAINBOW:
			retval = "Rainbow full art";
			break;
		case MISPRINT:
			retval = "Misprint";
			break;
		}

		return retval;
	}

	public void setProperty(int property) {
		if(property >= 0 && property <= 5) {
			this.property = property;
		} else {
			this.property = 0;
		}
	}
	
	public int setProperty(String property) {
		int retVal = 0;
		
		switch(property) {
		case "Normal":
			retVal = NORMAL;
			break;
		case "Reverse Holo":
			retVal = REVERSE_HOLO;
			break;
		case "Holo":
			retVal = HOLO;
			break;
		case "Full art":
			retVal = FULL_ART;
			break;
		case "Rainbow full art":
			retVal = RAINBOW;
			break;
		case "Misprint":
			retVal = MISPRINT;
			break;
		}
		
		return retVal;
	}

	@Override
	public String toString() {
		return cardName + ":, " + setName + " #" + cardNo + " (" + property + ")";
	}
}
