package com.cgtrader.spring.backend;

public interface SetInfoFactory {

	//FIXME: bugfix improper polymorphism implementation; cleanup code
	// Method bodies retained in case of future implementation switch

	public int getSetSize(int setNumber);
	public int getSetSize(String setName);
	public String getSetName(int setNumber);
	public int getSetNumber(String setName);

	/*
	//return the number of cards in a given set by set number
	public int getSetSize(int setNumber);
	{
		int retVal = 0;

		if(setNumber >0 && setNumber < setSizes.size()) {
			retVal = setSizes.get(setNumber);
		}

		return retVal;
	}


	public int getTotalSets();
{
		return totalSets;
	}

	//return the number of cards in a given set by name
	public static int getSetSize(String setName) 
	{
		int retVal = 0;
		int index = 0;
		boolean found = false;

		while (index<setSizes.size() && !found){
			String searchVal = setNames.get(index);
			if(setName.equals(searchVal)) {
				retVal = setSizes.get(index);
				found = true;
			}
		}

		if(!found) {
			retVal = 0;
			Notification.show("Invalid set");
		}

		return retVal;
	}

	//return the name of a set by number
	public String getSetName(int setNumber) 
	{
		String retVal = "";

		if(setNumber > 0 && setNumber < setNames.size()) {
			retVal = setNames.get(setNumber);
		}

		return retVal;
	}

	//return the number of a set by name
	public int getSetNumber(SetInfoFactory infoFact, String setName)
	{
		int retVal = 0;
		int index = 1;
		boolean found = false;
		String searchName = infoFact.getSetName(0);

		//Search through the list of set names for a match, then copy
		while(!found && index < setNames.size()) {
			if(searchName.equals(setName)) {
				found = true;
				retVal = index;
			}
			else {
				index++;
				searchName = infoFact.getSetName(index);
			}
		}
		return retVal;
	}
	 */
}
