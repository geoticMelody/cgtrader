package com.cgtrader.spring.views.portfolioview;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import com.cgtrader.spring.backend.BackendService;
import com.cgtrader.spring.backend.Card;
import com.vaadin.flow.component.AbstractField;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.listbox.ListBox;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.splitlayout.SplitLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.data.provider.QuerySortOrder;
import com.vaadin.flow.router.AfterNavigationEvent;
import com.vaadin.flow.router.AfterNavigationObserver;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

import com.cgtrader.spring.MainView;

@Route(value = "portfolioview", layout = MainView.class)
@PageTitle("PortfolioView")
@CssImport("styles/views/portfolioview/portfolio-view-view.css")
public class PortfolioViewView extends Div implements AfterNavigationObserver {
	private static final long serialVersionUID = 7848737773147113244L;

	@Autowired
    private BackendService cardsPortfolio;

    private Card currentCard;
    private Grid<Card> cards;

    //Form layout variables
    private TextField cardName = new TextField();
    private TextField setName = new TextField();
    private TextField cardNo = new TextField();
    private ListBox<String> TCGList = new ListBox<>();
    private ListBox<String> propertyList = new ListBox<>();
    
    //Card value variables
    private String sTCGName;
    private int iProperty; //Default is Normal in all factories

    //Buttons
    private Button clear = new Button("Clear");
    private Button delete = new Button("Delete");
    private Button save = new Button("Save");

    private Binder<Card> binder;

    public PortfolioViewView() {
        setId("portfolio-view-view");
        
        // Configure Grid
        cards = new Grid<>();
        cards.addThemeVariants(GridVariant.LUMO_NO_BORDER,
        		GridVariant.LUMO_ROW_STRIPES);

        //Column adding
        cards.addColumn(Card::getGameName).setHeader("Game");
        cards.addColumn(Card::getSetNumber).setHeader("Set #")
        	.setSortOrderProvider(direction -> Stream.of(
        			new QuerySortOrder("setNumber", direction)));
        cards.addColumn(Card::getSetName).setHeader("Set name")
        	.setSortProperty("setNumber")
        	.setSortOrderProvider(direction -> Stream.of(
        			new QuerySortOrder("setNumber", direction)));
        cards.addColumn(Card::getCardNo).setHeader("Card #");
        cards.addColumn(Card::getCardName).setHeader("Card name");
        cards.addColumn(Card::getProperty).setHeader("Property");
        cards.setHeightFull();

        //when a row is selected or deselected, populate form and set current selection
        cards.asSingleSelect().addValueChangeListener(event -> {
        		populateForm(event.getValue());
        		currentCard = cards.asSingleSelect().getValue();
        		//if(currentCard != null){
        			//Bugfix code
        			//Notification.show("Card set number: " + currentCard.getSetNumber());
        		//}
        	}
        );

        // Configure Form
        binder = new Binder<>(Card.class);
        
        TCGList.setItems("Pokémon");
        TCGList.setValue("Pokémon");
        TCGList.addValueChangeListener(event -> {
        	sTCGName = TCGList.getValue();
        });
        
    	propertyList.setItems("Normal", 
    			"Reverse Holo", 
    			"Holo",
    			"Full art", 
    			"Rainbow", 
    			"Misprint (holo bleed, miscenter, etc)" // TODO: [2] add specific codes for each type of misprint, esp. Shadowless
    			); 
    	propertyList.setValue("Normal"); //Initial value
    	propertyList.addValueChangeListener(event -> {
    		iProperty = getPropertyFromString(propertyList.getValue());
    	});
    	
    	
    	setName.addValueChangeListener(event -> {
    		setName.getValue();
    	});
    	
    	cardName.addValueChangeListener(event -> {
    		cardName.getValue();
    	});
    	
    	cardNo.addValueChangeListener(event -> {
    		cardNo.getValue();
    	});

        // Bind fields. This where you'd define e.g. validation rules
        
        binder.bind(
        		cardName, 
        		Card::getCardName, 
        		Card::setCardName);
        
        binder.bind(
        		setName,
        		Card::getSetName,
        		Card::setSetName);
        
        binder.bind(
        		cardNo,
        		Card::getCardNo,
        		Card::setCardNo);
        
        //TODO: [0] refactor how properties are handled in Card.java

        // the grid valueChangeEvent will clear the form too
        clear.addClickListener(e -> cards.asSingleSelect().clear());
        delete.addClickListener(e -> handleDeleteClickListener(!(currentCard == null)));
        save.addClickListener(e -> handleSaveClickListener(binder));

        SplitLayout splitLayout = new SplitLayout();
        splitLayout.setSizeFull();

        createGridLayout(splitLayout);
        createEditorLayout(splitLayout);

        add(splitLayout);
    }
    
    //Delete the selected card from the backend service
    private void handleDeleteClickListener(boolean cardExists) {    	
    	if (cardExists) {
			String deletName = currentCard.getCardName();
			cards.setDetailsVisible(currentCard, false);
			cards.getDataProvider().refreshAll();
			cardsPortfolio.deleteCard(currentCard);
			currentCard = null;
			Notification.show("Card " + deletName + " has been deleted.");
		}
    	else{
    		Notification.show("Multi-card selection coming Soon(TM)");
    	}
    }
    
    //Update a card in the backend service
    private void handleSaveClickListener(Binder<Card> binder) {
    	try {
			binder.writeBean(currentCard);
			currentCard.setProperty(iProperty); //writeBean doesn't change this for some reason
			cards.getDataProvider().refreshAll();
			Notification.show("Card edited");
		} catch (ValidationException e) {
			// TODO Auto-generated catch block
			Notification.show("Error editing card");
			e.printStackTrace();
		} catch (NullPointerException e) {
			Notification.show("Please select a card to save.");
		}
    }
    
  //TODO: [0] make this part of the Card class somehow
    // or change the ListBox to return ints instead of strings
    private int getPropertyFromString(String property) {
    	int retval = 0;
    	
    	if(property.equals("Normal"))
    		retval = Card.NORMAL;
    	else if(property.equals("Reverse Holo"))
    		retval = Card.REVERSE_HOLO;
    	else if(property.equals("Holo"))
    		retval = Card.HOLO;
    	else if(property.equals("Full art"))
    		retval = Card.FULL_ART;
    	else if(property.equals("Rainbow"))
    		retval = Card.RAINBOW;
    	else if(property.equals("Misprint (holo bleed, miscenter, etc)"))
    		retval = Card.MISPRINT;
    	else
    		retval = 0;
    	
    	return retval;
    }

    private void createEditorLayout(SplitLayout splitLayout) {
    	Div editorDiv = new Div();
        editorDiv.setId("editor-layout");
        FormLayout formLayout = new FormLayout();
        addFormItem(editorDiv, formLayout, TCGList, "Game name");
        addFormItem(editorDiv, formLayout, cardName, "Card name");
        addFormItem(editorDiv, formLayout, setName, "Set name");
        addFormItem(editorDiv, formLayout, cardNo, "Card number");
        addFormItem(editorDiv, formLayout, propertyList, "Property");
        createButtonLayout(editorDiv);
        splitLayout.addToSecondary(editorDiv);
    }

    private void createButtonLayout(Div editorDiv) {
        HorizontalLayout buttonLayout = new HorizontalLayout();
        buttonLayout.setId("button-layout");
        buttonLayout.setWidthFull();
        buttonLayout.setSpacing(true);
        clear.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
        delete.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
        save.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        buttonLayout.add(clear, delete, save);
        editorDiv.add(buttonLayout);
    }

    private void createGridLayout(SplitLayout splitLayout) {
        Div wrapper = new Div();
        wrapper.setId("wrapper");
        wrapper.setWidthFull();
        splitLayout.addToPrimary(wrapper);
        wrapper.add(cards);
    }

    private void addFormItem(Div wrapper, FormLayout formLayout,
            AbstractField field, String fieldName) {
        formLayout.addFormItem(field, fieldName);
        wrapper.add(formLayout);
        field.getElement().getClassList().add("full-width");
    }

    @Override
    public void afterNavigation(AfterNavigationEvent event) {

        // Lazy init of the grid items, happens only when we are sure the view will be
        // shown to the user
        cards.setItems(cardsPortfolio.getPortfolio());
    }

    private void populateForm(Card value) {
        // Value can be null as well, that clears the form
        binder.readBean(value);
    }
}
