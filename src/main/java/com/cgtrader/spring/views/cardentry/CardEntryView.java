package com.cgtrader.spring.views.cardentry;

import com.cgtrader.spring.backend.BackendService;
import com.cgtrader.spring.backend.Card;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.NativeButton;
import com.vaadin.flow.component.listbox.ListBox;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteAlias;

import org.springframework.beans.factory.annotation.Autowired;

import com.cgtrader.spring.MainView;

@Route(value = "cardentry", layout = MainView.class)
@RouteAlias(value = "", layout = MainView.class)
@PageTitle("CardEntry")
@CssImport("styles/views/cardentry/card-entry-view.css")
public class CardEntryView extends Div {
	private static final long serialVersionUID = -3831481034519699139L;
	
	//Backend service that holds the cards + communicates with database
	@Autowired
	private BackendService cardsPortfolio;
	
	//Form layout variables
	private ListBox<String> tcgNameList = new ListBox<>();
    private TextField cardName = new TextField(); //TODO: [0] MUCH later, convert into ListBox using Factory subsets
    private TextField setName = new TextField(); //TODO: [3] convert into ListBox using factory to fill
    private TextField cardNo = new TextField();    
    private ListBox<String> propertyList = new ListBox<>(); //ListBox must be of type List<String>, gonna have to convert to int in the code
    
    //Card value variables
    private String sTCGName = "Pokémon"; //TODO: [1] instantiate from database
    private String sCardName = "";
    private String sSetName  = "";
    private String sCardNo = "";
    private int iProperty = 0; //Default is Normal in all factories
    
    private Card newCard;
    
    //Buttons
    private Button clear = new Button("Clear");
    private Button addCard = new Button("Add Card");

    public CardEntryView() {
        setId("card-entry-view");
        VerticalLayout wrapper = createWrapper();

        createTitle(wrapper);
        createFormLayout(wrapper);
        createButtonLayout(wrapper);

        // Configure Form
        Binder<Card> binder = new Binder<>(Card.class);
        
        tcgNameList.setItems("Pokemon"); //TODO: [0] add more games here!
        tcgNameList.setValue("Pokemon"); //Initial value
        tcgNameList.addValueChangeListener(event -> {
        		sTCGName = tcgNameList.getValue();
        });
        
    	propertyList.setItems("Normal", 
    			"Reverse Holo", 
    			"Holo",
    			"Full art", 
    			"Rainbow", 
    			"Misprint (holo bleed, miscenter, etc)" // TODO: [2] add specific codes for each type of misprint, esp. Shadowless
    			);
    	
    	propertyList.addValueChangeListener(event -> {
    		iProperty = getPropertyFromString(propertyList.getValue());
    	});
    	
    	
    	setName.addValueChangeListener(event -> {
    		sSetName = setName.getValue();
    	});
    	
    	cardName.addValueChangeListener(event -> {
    		sCardName = cardName.getValue();
    	});
    	
    	cardNo.addValueChangeListener(event -> {
    		sCardNo = cardNo.getValue();
    	});
    	
        // Bind fields. This where you'd define e.g. validation rules
        
        binder.bind(
        		setName,
        		Card::getSetName,
        		Card::setSetName);
        
        binder.bind(
        		setName,
        		Card::getSetName,
        		Card::setSetName);
        
        binder.bind(
        		cardNo,
        		Card::getCardNo,
        		Card::setCardNo);
        
        /*
        binder.bind(
        		iProperty,
        		Card::getProperty,
        		Card::setProperty);
        		*/
        
        
    	//TODO: [2] implement scrolling ListBox for set name	 
    	
        clear.addClickListener(e -> handleClearClickListener(binder));
    	addCard.addClickListener(e -> handleSaveClickListener(binder));

        add(wrapper);
    }
    
    private void handleClearClickListener(Binder<Card> binder) {
    	binder.readBean(null);
    }
    
    private void handleSaveClickListener(Binder<Card> binder) {
    	//Create a new card and add it to the database
    	newCard = new Card(sTCGName, sCardName, sSetName, sCardNo, iProperty);
    	try {    				
    		binder.writeBean(newCard);
    		cardsPortfolio.addCard(newCard);
        	Notification.show("Card added to database.");
    	}
    	catch(NullPointerException e) {
    		Notification.show("Please enter all values.");
    	}
    	catch(ValidationException e) {
    		Notification.show("Validation exception thrown.");
    	}
    	
    }
    
    //TODO: [0] make this part of the Card class somehow
    // or change the ListBox to return ints instead of strings
    private int getPropertyFromString(String property) {
    	int retval = 0;
    	
    	if(property.equals("Normal"))
    		retval = Card.NORMAL;
    	else if(property.equals("Reverse Holo"))
    		retval = Card.REVERSE_HOLO;
    	else if(property.equals("Holo"))
    		retval = Card.HOLO;
    	else if(property.equals("Full art"))
    		retval = Card.FULL_ART;
    	else if(property.equals("Rainbow"))
    		retval = Card.RAINBOW;
    	else if(property.equals("Misprint (holo bleed, miscenter, etc)"))
    		retval = Card.MISPRINT;
    	else
    		retval = 0;
    	
    	return retval;
    }

    private void createTitle(VerticalLayout wrapper) {
    	NativeButton moar = new NativeButton("More games coming soon!",
        		event -> Notification.show("Join our Discord to suggest new games!"));
    	 //moar.applyThemeVariants(ButtonVariant.LUMO_TERTIARY);
    	H1 h1 = new H1(tcgNameList, moar);
        wrapper.add(h1);
    }

    private VerticalLayout createWrapper() {
        VerticalLayout wrapper = new VerticalLayout();
        wrapper.setId("wrapper");
        wrapper.setSpacing(false);
        return wrapper;
    }

    private void createFormLayout(VerticalLayout wrapper) {	
        FormLayout formLayout = new FormLayout();
        addFormItem(wrapper, formLayout, setName, "Set name");
        addFormItem(wrapper, formLayout, cardName, "Card name");
        addFormItem(wrapper, formLayout, cardNo, "Card number");
        addFormItem(wrapper, formLayout, propertyList, "Property");
        
        formLayout.setColspan(setName, 1);
        formLayout.setColspan(cardName, 1);
        formLayout.setColspan(cardNo, 1);
        formLayout.setColspan(propertyList, 1);

    }

    private void createButtonLayout(VerticalLayout wrapper) {
        HorizontalLayout buttonLayout = new HorizontalLayout();
        buttonLayout.addClassName("button-layout");
        buttonLayout.setWidthFull();
        buttonLayout
                .setJustifyContentMode(FlexComponent.JustifyContentMode.END);
        clear.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
        addCard.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        buttonLayout.add(clear);
        buttonLayout.add(addCard);
        wrapper.add(buttonLayout);
    }

    private FormLayout.FormItem addFormItem(VerticalLayout wrapper,
            FormLayout formLayout, Component field, String fieldName) {
        FormLayout.FormItem formItem = formLayout.addFormItem(field, fieldName);
        wrapper.add(formLayout);
        field.getElement().getClassList().add("full-width");
        return formItem;
    }

}
