package com.cgtrader.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 * The entry point of the Spring Boot application.
 */
@SpringBootApplication
public class Application extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
    
    /*TODO codes:
     * [0]: lowest possible priority, app works fine without addressing it
     * [1]: highest possible priority, necessary for essential functionality
     * [2]: second highest priority, QoL improvements
     * [3]: set factory fixes

     */
}
